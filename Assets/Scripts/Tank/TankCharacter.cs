﻿using UnityEngine;
using System.Collections;

namespace Tank
{
    public class TankCharacter : MonoBehaviour
    {
      
        private int health;
        private int armor;    

        private void OnTriggerEnter2D(Collider2D other)
        {            
            if(other.tag == "enemy")
            {
              int _damage = other.GetComponent<enemy.EnemyBehaviour>().DamageToTank();
                _damage -= armor;
                if (_damage > 0) {
                    health -=  _damage;
                }
            }           
        }
    }
}
