﻿using UnityEngine;
using System.Collections;
using Manager;

namespace Tank
{
    public class TankMove : MonoBehaviour
    {
     
        private float SpeedMove = 0.02f;
        private float SpeedRotateTank =1.5f;

        private Rigidbody2D myRigidbody;
        private Transform myTransform;

        // Use this for initialization
        private void Awake()
        {
            myTransform = transform;
            myRigidbody = GetComponent<Rigidbody2D>();
            InputManager.OnClicked += Movement;
        }

        private void OnDisable()
        {
            InputManager.OnClicked -= Movement;
        }     

        private void Movement(eKey eKeyKode)
        {         
            if (eKeyKode == eKey.forward)
            {
                myRigidbody.position += new Vector2(transform.right.x, transform.right.y)  * SpeedMove;
            }
            if (eKeyKode == eKey.back)
            {
                myRigidbody.position -= new Vector2(transform.right.x, transform.right.y) * SpeedMove;
            }
            if (eKeyKode == eKey.left)
            {
                myTransform.Rotate(0, 0, SpeedRotateTank);
            }
            if (eKeyKode == eKey.right)
            {
                myTransform.Rotate(0, 0, -SpeedRotateTank);
            }
        }    
       
    }
}
