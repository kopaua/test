﻿using UnityEngine;
using System.Collections.Generic;
using Manager;
using States;
namespace Tank {
    public class TankTower : MonoBehaviour
    {     
        public ITurretState currentState;
        public BigGunState bigGunState;
        public SmallGunState smallGunState;

        // Use this for initialization
        void Start()
        {
            var tank = GetComponent<TankTower>();
            bigGunState.SetTurret(ref tank);
            smallGunState.SetTurret(ref tank);
            currentState = bigGunState;
            currentState.ToBigGunState();
            InputManager.OnClicked += SwapWeapon;
        }

        private void OnDisable()
        {
            InputManager.OnClicked -= SwapWeapon;
        }     

        private void SwapWeapon(eKey eKeyKode)
        {
            if (eKeyKode == eKey.fire)
            {
                currentState.FireTurret();
            }
            if (eKeyKode == eKey.weaponBig)
            {
                currentState.ToBigGunState();
            }
            else if (eKeyKode == eKey.weaponEasy)
            {
                currentState.ToSmallGunState();
            }
        }
    }
}
