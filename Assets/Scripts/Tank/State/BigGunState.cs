﻿using UnityEngine;
using System.Collections.Generic;
using Tank;

namespace States
{
    public class BigGunState : MonoBehaviour, ITurretState
    {
        [SerializeField]
        public Transform startBullet;
        [SerializeField]
        public GameObject prefabBullet;

        private List<GameObject> bullets = new List<GameObject>();
        private int bulletsCount = 1;
        private int damage = 50;

        private TankTower turret;


        public void SetTurret(ref TankTower state)
        {
            turret = state;
        }

        // Use this for initialization
        void Start()
        {
            for (int i=0;i< bulletsCount;i++)
            {
              GameObject obj = Instantiate(prefabBullet) as GameObject;
              obj.SetActive(false);
              Bullet _bullet = obj.GetComponent<Bullet>();
              _bullet.damage = damage;
              bullets.Add(obj);
            }
        }

        public void FireTurret()
        {
            for (int i = 0; i < bulletsCount; i++)
            {
                if (!bullets[i].activeInHierarchy)
                {                   
                    bullets[i].transform.rotation = startBullet.root.rotation;
                    bullets[i].transform.position = startBullet.position;
                    bullets[i].SetActive(true);
                    break;
                }
            }
        }

        public void ToBigGunState()
        {            
            turret.currentState = turret.bigGunState;
            startBullet.gameObject.SetActive(true);
        }

        public void ToSmallGunState()
        {           
            turret.currentState = turret.smallGunState;
            turret.currentState.ToSmallGunState();
            startBullet.gameObject.SetActive(false);
        }     
    }
}
