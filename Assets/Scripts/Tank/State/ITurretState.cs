﻿
namespace States
{
    public interface ITurretState
    {

        void ToBigGunState();

        void ToSmallGunState();

        void FireTurret();
    }
}
