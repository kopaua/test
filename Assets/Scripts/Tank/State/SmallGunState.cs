﻿using UnityEngine;
using System.Collections.Generic;
using Tank;

namespace States
{
    public class SmallGunState : MonoBehaviour,ITurretState
    {
        [SerializeField]
        public Transform startBullet;
        [SerializeField]
        public GameObject prefabBullet;

        private List<GameObject> bullets = new List<GameObject>();
        private int bulletsCount = 10;
        private int damage = 20;

        private TankTower turret;

        public void SetTurret(ref TankTower state)
        {
            turret = state;
        }

        // Use this for initialization
        void Start()
        {
            for (int i = 0; i < bulletsCount; i++)
            {
                GameObject obj = Instantiate(prefabBullet) as GameObject;
                obj.SetActive(false);
                Bullet _bullet = obj.GetComponent<Bullet>();
                _bullet.damage = damage;
                bullets.Add(obj);
            }
        }

        public void FireTurret()
        {
            for (int i = 0; i < bulletsCount; i++)
            {
                if (!bullets[i].activeInHierarchy)
                {
                    bullets[i].transform.rotation = startBullet.root.rotation;
                    bullets[i].transform.position = startBullet.position;
                    bullets[i].SetActive(true);
                    break;
                }
            }
        }

        public void ToBigGunState()
        {           
            turret.currentState = turret.bigGunState;
            turret.currentState.ToBigGunState();
            startBullet.gameObject.SetActive(false);
        }

        public void ToSmallGunState()
        {          
            turret.currentState = turret.smallGunState;
            startBullet.gameObject.SetActive(true);
        }
    }
}
