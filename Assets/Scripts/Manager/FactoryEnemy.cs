﻿using UnityEngine;
using System.Collections;

namespace Manager {
    public class FactoryEnemy : MonoBehaviour
    {

        [SerializeField]
        private GameObject prafebEnemyRandom;


        public GameObject Create<T>() where T : MonoBehaviour
        {
            GameObject _enemy = Instantiate(prafebEnemyRandom) as GameObject;
            _enemy.AddComponent<T>();
            return _enemy;
        }
    }	
}
