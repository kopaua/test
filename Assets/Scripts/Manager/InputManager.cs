﻿using UnityEngine;
using System.Collections;

namespace Manager
{
    public enum eKey
    {
        forward,
        back,
        left,
        right,
        fire,
        weaponBig,
        weaponEasy
    }  

    public class InputManager : MonoBehaviour
    {

        public delegate void ClickAction(eKey eKey);
        public static event ClickAction OnClicked;    

        // Update is called once per frame
        private void Update()
        {
            if (OnClicked != null)
            {
                Move();
                Fire();
                SwapWeapon();
            }
        }

        private void FixedUpdate()
        {
            if (OnClicked != null)
            {
                if (Input.GetKey(KeyCode.UpArrow))
                {
                    OnClicked(eKey.forward);
                }
                if (Input.GetKey(KeyCode.DownArrow))
                {
                    OnClicked(eKey.back);
                }
            }
        }

        private void Fire()
        {
            if (Input.GetKeyDown(KeyCode.X))
            {
                OnClicked(eKey.fire);
            }
        }

        private void SwapWeapon()
        {
            if (Input.GetKeyDown(KeyCode.Q))
            {
                OnClicked(eKey.weaponBig);
            }
            if (Input.GetKeyDown(KeyCode.W))
            {
                OnClicked(eKey.weaponEasy);
            }
        }
    

        private void Move()
        {        
            if (Input.GetKey(KeyCode.RightArrow))
            {
                OnClicked(eKey.right);
            }
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                OnClicked(eKey.left);
            }
        }
    }

}
