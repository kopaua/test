﻿using UnityEngine;
using System.Collections.Generic;
using enemy;

namespace Manager {

    public class SpawnEnemy : MonoBehaviour {

        public static SpawnEnemy Instance { get; private set; }
        private int maxEnemyOnScene = 10;
        private List<GameObject> enemiesPull = new List<GameObject>();
        private FactoryEnemy factoryEnemy;
        private float borderX = 5f;
        private float borderY = 4f;

        // Use this for initialization
        void Start()
        {
            Singelton();
            factoryEnemy = GetComponent<FactoryEnemy>();
            CreateEnemies();
        }

        public void DeactivateEnemy(GameObject _enemy)
        {          
            _enemy.SetActive(false);                 
            ToRandomPosition(_enemy.transform);
            _enemy.SetActive(true);
        }

        private void Singelton()
        {
            if (Instance == null)
                Instance = this;
            else if (Instance != this)
                Destroy(gameObject);
        }

        private void CreateEnemies()
        {
            for (int i = 0; i < maxEnemyOnScene; i++)
            {
                var _enemy = factoryEnemy.Create<EnemyBehaviour>();
                RandomStatsForEnemy(_enemy.GetComponent<EnemyBasic>());
                enemiesPull.Add(_enemy);
            }
        }

        private void RandomStatsForEnemy(EnemyBasic _enemy)
        {
            _enemy.health = Random.Range(10, 50);
            _enemy.damage = Random.Range(1, 10);
            _enemy.speedMove = Random.Range(0.1f, 0.5f);
            _enemy.armor = Random.Range(0, 5);
            _enemy.GetComponent<SpriteRenderer>().color = Color.HSVToRGB(Random.Range(0.01f, 0.99f), Random.Range(0.01f, 0.99f), Random.Range(0.01f, 0.99f));
            ToRandomPosition(_enemy.transform);
        }

        private void ToRandomPosition(Transform _obj)
        {         
            float posTop = 0; 
            float posLeft = 0;
            if (Random.Range(0, 100) >50)
            {
                posLeft = Random.Range(-borderY , borderY );
                posTop = borderX ;
                if (Random.Range(0, 100) > 50)
                    posTop = -posTop;              
            }
            else
            {
                posTop = Random.Range(-borderX , borderX );
                posLeft = borderY;
                if (Random.Range(0, 100) > 50)
                    posLeft = -posLeft;
            }
            Vector3 newPos = new Vector3(posTop, posLeft, 0);
            _obj.position = newPos;
        }
        
    }
}
