﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {

    public int damage;

    private float speedMove = 0.1f;   
    private int timeLife =2;
    private Rigidbody2D myRigidbody;

    private void Awake()
    {
        myRigidbody = GetComponent<Rigidbody2D>();
    }

    private void OnEnable()
    {
        Invoke("Deactivate", timeLife);
    }  

	// Update is called once per frame
	void FixedUpdate () {
        myRigidbody.position += new Vector2(transform.right.x, transform.right.y) * speedMove;
    }

    private void Deactivate()
    {      
        transform.gameObject.SetActive(false);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "enemy")
        {
             var _enemy = other.GetComponent<enemy.EnemyBehaviour>();
            _enemy.GetDamageFromTank(damage);
            Deactivate();
        }
    }
}
