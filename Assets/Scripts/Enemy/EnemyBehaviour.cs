﻿using UnityEngine;
using System.Collections;
using Manager;

namespace enemy
{
    public class EnemyBehaviour : EnemyBasic    {

        private Transform target;
        private Transform myTransform;

        // Use this for initialization
        void Start()
        {
            myTransform = transform;
            target = FindObjectOfType<Tank.TankMove>().transform;
        }       

        // Update is called once per frame
        void Update()
        {
            myTransform.position = Vector3.MoveTowards(myTransform.position, target.position, speedMove * Time.deltaTime);
        }

        public void GetDamageFromTank(int _damage)
        {
            _damage -= armor;
            if (_damage > 0)
                health -= _damage;
            if(health <= 0)
                SpawnEnemy.Instance.DeactivateEnemy(gameObject);
        }

        public int DamageToTank()
        {
            SpawnEnemy.Instance.DeactivateEnemy(gameObject);
            return damage;
        }
    }
}
