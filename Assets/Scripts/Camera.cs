﻿using UnityEngine;
using System.Collections;

public class Camera : MonoBehaviour {

    [SerializeField]
    private Transform target;

    private Transform myTransform;
    private Vector3 offset = new Vector3(0,0,-10);  

    void Start()
    {
        myTransform = transform;
    }

    // Update is called once per frame
    void LateUpdate () {
        myTransform.position = target.position + offset;
    }
}
